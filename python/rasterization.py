from transformations import Transformations2D as t2d

class SlopeTransform(object):

    @staticmethod
    def normalize(p0, p1):
        pp0 = p0
        pp1 = p1

        pp0x,pp0y = pp0
        pp1x,pp1y = pp1
        invX = pp0x > pp1x
        invY = pp0y > pp1y

        if invX:
            pp0 = t2d.invertX(pp0)
            pp1 = t2d.invertX(pp1)

        if invY:
            pp0 = t2d.invertY(pp0)
            pp1 = t2d.invertY(pp1)

        pp0x,pp0y = pp0
        pp1x,pp1y = pp1
        if (pp1x-pp0x) == 0:
            swapXY = True
        else:
            swapXY = ((pp1y-pp0y) / (pp1x-pp0x)) > 1

        if swapXY:
            pp0 = t2d.swapXY(pp0)
            pp1 = t2d.swapXY(pp1)

        return ((pp0,pp1), invX, invY, swapXY)

    @staticmethod
    def deNormalize(p, invX,invY, swapXY):
        pp = p

        if swapXY:
            pp = t2d.swapXY(pp)
        if invY:
            pp = t2d.invertY(pp)
        if invX:
            pp = t2d.invertX(pp)

        return pp

class NoOptim(object):

    @staticmethod
    def rasterize(p0,p1):
        (pp0,pp1), invX,invY,swapXY = SlopeTransform.normalize(p0,p1)
        pp0x,pp0y = pp0
        pp1x,pp1y = pp1

        if (pp1x-pp0x) == 0:
            return [pp0x]
        m = (pp1y-pp0y) / (pp1x-pp0x)
        b = pp0y - (m*pp0x)

        outList = []
        for x in range(pp0x,pp1x+1):
            p = (x, round(m*x + b))
            pp = SlopeTransform.deNormalize(p, invX, invY, swapXY)
            outList.append(pp)

        return outList

class DDA(object):

    @staticmethod
    def rasterize(p0,p1):
        (pp0,pp1), invX,invY,swapXY = SlopeTransform.normalize(p0,p1)
        pp0x,pp0y = pp0
        pp1x,pp1y = pp1

        if (pp1x-pp0x) == 0:
            return [pp0x]
        m = (pp1y-pp0y) / (pp1x-pp0x)

        outList = [SlopeTransform.deNormalize(pp0, invX, invY, swapXY)]
        _,yi = pp0
        for xi in range(pp0x+1, pp1x+1):
            yi += m
            p = (xi, round(yi))
            pp = SlopeTransform.deNormalize(p, invX, invY, swapXY)
            outList.append(pp)

        return outList


class Bresenham(object):

    @staticmethod
    def rasterizeNoOptim(p0,p1):
        (pp0,pp1), invX,invY,swapXY = SlopeTransform.normalize(p0,p1)
        pp0x,pp0y = pp0
        pp1x,pp1y = pp1

        dx = pp1x-pp0x
        dy = pp1y-pp0y

        # > 0 if x,y is below line (defined by dx, dy and p0)
        def F(x,y):
            a = dy
            b = -dx
            c = -(dy*pp0x -dx*pp0y)
            return a*x + b*y + c

        outList = [SlopeTransform.deNormalize(pp0, invX, invY, swapXY)]

        # decision variable (initilization)
        d = F(pp0x+1, pp0y+0.5)
        yi = pp0y
        for xi in range(pp0x+1, pp1x+1):
            if d > 0:
                yi += 1

            d = F(xi+1, yi+0.5)

            p = (xi, yi)
            pp = SlopeTransform.deNormalize(p, invX, invY, swapXY)
            outList.append(pp)

        return outList

    @staticmethod
    def rasterizeMathOptim(p0,p1):
        (pp0,pp1), invX,invY,swapXY = SlopeTransform.normalize(p0,p1)
        pp0x,pp0y = pp0
        pp1x,pp1y = pp1

        dx = pp1x-pp0x
        dy = pp1y-pp0y

        a = dy
        b = -dx
        c = -(dy*pp0x -dx*pp0y)

        outList = [SlopeTransform.deNormalize(pp0, invX, invY, swapXY)]

        # decision variable (initilization)
        d = a + (b*0.5)
        yi = pp0y
        for xi in range(pp0x+1,pp1x+1):
            if d > 0:
                yi += 1
                d += a+b
            else:
                d += a

            p = (xi,yi)

            pp = SlopeTransform.deNormalize(p, invX, invY, swapXY)
            outList.append(pp)

        return outList

    @staticmethod
    def rasterize(p0, p1):
        (pp0,pp1), invX,invY,swapXY = SlopeTransform.normalize(p0,p1)
        pp0 = [int(p) for p in pp0]
        pp1 = [int(p) for p in pp1]
#        pp0x,pp0y = pp0
#        pp1x,pp1y = pp1
#
#        dx = pp1x-pp0x
#        dy = pp1y-pp0y
#
#        a = dy
#        a2 = 2*a
#        b = -dx
#        b2 = 2*b
        a = pp1[1]-pp0[1]
        a2 = 2*a
        b = pp0[0]-pp1[0]
        b2 = 2*b

        outList = []
        # decision variable (initilization)
        d = a2 + b
        xi,yi = pp0
        for xi in range(pp0[0],pp1[0]+1):
            p = (xi,yi)
            pp = SlopeTransform.deNormalize(p, invX, invY, swapXY)
            outList.append(pp)
            if d > 0:
                yi += 1
                d += a2+b2
            else:
                d += a2

        return outList

class LineRasterization(object):

    @staticmethod
    def rasterize(p0,p1):
        return Bresenham.rasterize(p0,p1)

class PinedaRasterization(object):

    @staticmethod
    def rasterize(polygon):
        pass

class PolygonRasterization(object):

    # polygon must be convex!
    @staticmethod
    def rasterize(polygon):
        return PinedaRasterization.rasterize(polygon)


if __name__ == "__main__":
    from screen import Screen
    import random
    import time
    import signal
    import sys

    sizex,sizey = 90,50

    mode = "simulation"

    if mode == "simulation":

        screen = Screen(sizex,sizey)
        screen.initScreen()

        def sigintHandler(sig, frame):
            screen.closeScreen()
            sys.exit(0)
        signal.signal(signal.SIGINT, sigintHandler)


        while True:
            time.sleep(1)
            pixels = []
            for i in range(2):

                p0 = (random.randint(0,sizex-1),random.randint(0,sizey-1))
                p1 = (random.randint(0,sizex-1),random.randint(0,sizey-1))

#        pixels1 = NoOptim.rasterize(p0,p1)
#        pixels2 = DDA.rasterize(p0,p1)
#        pixels3 = Bresenham.rasterizeNoOptim(p0,p1)
#        pixels4 = Bresenham.rasterizeMathOptim(p0,p1)
                pixels5 = Bresenham.rasterize(p0,p1)

#        for pixels in [pixels1, pixels2, pixels3, pixels4, pixels5]:
#            for pix in pixels:
#                x,y = pix
#                screen.addPixel(x,y, 1)
                pixels += pixels5

            screen.setPixels(pixels)

            screen.draw()

    # benchmark
    if mode == "benchmark":

        funcs = dict()
        funcs["no optimization"]                = NoOptim.rasterize
        funcs["dda"]                            = DDA.rasterize
        funcs["bresenham no optimizations"]     = Bresenham.rasterizeNoOptim
        funcs["bresenham math optimization"]    = Bresenham.rasterizeMathOptim
        funcs["bresenham all optimizations"]    = Bresenham.rasterize

        for funcName,func in funcs.items():
            t0 = time.time()
            num = 10**5
            for i in range(num):
                p0 = (random.randint(0,sizex-1),random.randint(0,sizey-1))
                p1 = (random.randint(0,sizex-1),random.randint(0,sizey-1))
                pixels = func(p0,p1)
            print("{} : {}".format(funcName, time.time()-t0))

    # tests
    if mode == "tests":
        p0 = (random.randint(0,sizex-1),random.randint(0,sizey-1))
        p1 = (random.randint(0,sizex-1),random.randint(0,sizey-1))

        pixels1 = NoOptim.rasterize(p0,p1)
        pixels2 = DDA.rasterize(p0,p1)
        pixels3 = Bresenham.rasterizeNoOptim(p0,p1)
        pixels4 = Bresenham.rasterizeMathOptim(p0,p1)
        pixels5 = Bresenham.rasterize(p0,p1)

        for i in range(len(pixels3)):
            p1x,p1y = pixels1[i]
            p2x,p2y = pixels2[i]
            p3x,p3y = pixels3[i]
            t1 = p1x == p2x == p3x
            t2 = p1y == p2y == p3y
            if not t1 or not t2:
                if not t1 and not t2:
                    print("alert")

        screen = Screen(sizex,sizey)
        screen.initScreen()

        def sigintHandler(sig, frame):
            screen.closeScreen()
            sys.exit(0)
        signal.signal(signal.SIGINT, sigintHandler)

        for pixels in [pixels1, pixels2, pixels3, pixels4, pixels5]:
            for pix in pixels:
                x,y = pix
                screen.addPixel(x,y, 1)

        screen.draw()

        while True:
            time.sleep(1)
