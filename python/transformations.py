import numpy as np
import math

class Transformations2D(object):

    @staticmethod
    def invertX(p):
        px,py = p
        pnew = (-px,py)
        return pnew

    @staticmethod
    def invertY(p):
        px,py = p
        pnew = (px,-py)
        return pnew

    @staticmethod
    def swapXY(p):
        px,py = p
        pnew = (py,px)
        return pnew

class Transformations3D(object):

    @staticmethod
    def getIdentityTransform():
        return np.eye(4)

    @staticmethod
    def getTranslationTransform(x,y,z):
        return np.array([
            [ 1, 0, 0, x],
            [ 0, 1, 0, y],
            [ 0, 0, 1, z],
            [ 0, 0, 0, 1]])

    @staticmethod
    def getRotationTransform(phi, axis="z"):
        c = math.cos(phi * math.pi/180)
        s = math.sin(phi * math.pi/180)

        if axis == "z":
            return np.array([
                [  c, -s,  0,  0],
                [  s,  c,  0,  0],
                [  0,  0,  1,  0],
                [  0,  0,  0,  1]])

        if axis == "y":
            return np.array([
                [  c,  0,  s,  0],
                [  0,  1,  0,  0],
                [ -s,  0,  c,  0],
                [  0,  0,  0,  1]])

        if axis == "x":
            return np.array([
                [  1,  0,  0,  0],
                [  0,  c, -s,  0],
                [  0,  s,  c,  0],
                [  0,  0,  0,  1]])

    @staticmethod
    def getLookAtTransform(eye,point, up=(0,1,0)):
        ex,ey,ez = eye
        px,py,pz = point
        ux,uy,uz = up
        d = (px-ex,py-ey,pz-ez)
        dx,dy,dz = d
        r = ((dy*uz)-(dz*uy), (dz*ux)-(dx*uz), (dx*uy)-(dy*ux))
        rx,ry,rz = r
        u = ((ry*dz)-(rz*dy), (rz*dx)-(rx*dz), (rx*dy)-(ry*dx))
        ux,uy,uz = u

        dNorm = math.sqrt((dx*dx) + (dy*dy) + (dz*dz))
        rNorm = math.sqrt((rx*rx) + (ry*ry) + (rz*rz))
        uNorm = math.sqrt((ux*ux) + (uy*uy) + (uz*uz))
        dx,dy,dz = dx/dNorm,dy/dNorm,dz/dNorm
        rx,ry,rz = rx/rNorm,ry/rNorm,rz/rNorm
        ux,uy,uz = ux/uNorm,uy/uNorm,uz/uNorm

        rot = np.array([
            [ rx, ry, rz, 0],
            [ ux, uy, uz, 0],
            [-dx,-dy,-dz, 0],
            [  0,  0,  0, 1]])

        trans = np.array([
            [   1,   0,   0, -ex],
            [   0,   1,   0, -ey],
            [   0,   0,   1, -ez],
            [   0,   0,   0,   1]])

        return rot @ trans

    @staticmethod
    def getFrustumTransform(phi, ratio, near, far):
        top = math.tan((phi/2) * (math.pi/180)) * near
        bottom = -top
        right = top * ratio
        left = bottom * ratio

        a = 2*near / (right-left)
        b = (right+left) / (right-left)
        c = 2*near / (top-bottom)
        d = (top+bottom) / (top-bottom)
        e = -1 * (far+near) / (far-near)
        f = -2*far*near / (far-near)

        return np.array([
            [  a,  0,  b,  0],
            [  0,  c,  d,  0],
            [  0,  0,  e,  f],
            [  0,  0, -1,  0]])

    @staticmethod
    def getViewportTransform(p, w, h):
        px,py = p
        # |   ux ,  uy ,  uz , -uT*m |
        # |   vx ,  vy ,  vz , -vT*m |
        # |    0 ,   0 ,   0 ,     1 |
        return np.array([
            [    w/2,      0,      0, w/2+px],
            [      0,    h/2,      0, h/2+py],
            [      0,      0,      0,      1]])

    @staticmethod
    def applyTransform(transform, point):
        x,y,z = point
        point4D = np.array([x,y,z,1])

        pp = transform @ point4D
        pp /= pp[-1]
        if len(pp) == 4:
            xN,yN,zN,_ = pp
            return [xN,yN,zN]
        elif len(pp) == 3:
            xN,yN,_ = pp
            return [xN,yN]
