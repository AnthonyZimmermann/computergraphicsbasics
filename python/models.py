from transformations import Transformations3D as t3d

class Rectangle(object):

    @staticmethod
    def calcModel(width,height):
        x0 = -width/2
        y0 = -height/2
        x1 = -x0
        y1 = -y0

        p0 = (x0,y0, 0)
        p1 = (x1,y0, 0)
        p2 = (x0,y1, 0)
        p3 = (x1,y1, 0)

        triangles = []
        triangles.append((p0,p2,p1))
        triangles.append((p2,p3,p1))
        return triangles

    @staticmethod
    def calcModelQuads(width,height):
        x0 = -width/2
        y0 = -height/2
        x1 = -x0
        y1 = -y0

        p0 = (x0,y0, 0)
        p1 = (x1,y0, 0)
        p2 = (x1,y1, 0)
        p3 = (x0,y1, 0)

        quads = []
        quads.append((p0,p1,p2,p3))
        return quads

class Cuboid(object):
    def __init__(self, width,height,depth, modelT=t3d.getIdentityTransform(), mode="quads"):
        if mode == "quads":
            self.model = Cuboid.calcModelQuads(width,height,depth)
        else:
            self.model = Cuboid.calcModel(width,height,depth)

        self.modelT = modelT

    def transform(self, modelT):
        self.modelT = modelT @ self.modelT

    @staticmethod
    def calcModel(width,height,depth):
        triangles = []

        t1 = t3d.getTranslationTransform(0,0,-depth/2)
        t2 = t3d.getTranslationTransform(0,0,depth/2)
        r1 = t3d.getRotationTransform(90, "y")
        r2 = t3d.getRotationTransform(90, "x")
        t3 = r1 @ t1
        t4 = r1 @ t2
        t5 = r2 @ t1
        t6 = r2 @ t2

        r = Rectangle.calcModel(width,height)

        for transform in [t1,t2,t3,t4,t5,t6]:
            for triangle in r:
                a,b,c = triangle
                aa = t3d.applyTransform(transform, a)
                bb = t3d.applyTransform(transform, b)
                cc = t3d.applyTransform(transform, c)
                triangles.append((aa,bb,cc))

        return triangles

    @staticmethod
    def calcModelQuads(width,height,depth):
        quads = []

        t1 = t3d.getTranslationTransform(0,0,-depth/2)
        t2 = t3d.getTranslationTransform(0,0,depth/2)
        r1 = t3d.getRotationTransform(90, "y")
        r2 = t3d.getRotationTransform(90, "x")
        t3 = r1 @ t1
        t4 = r1 @ t2
        t5 = r2 @ t1
        t6 = r2 @ t2

        r = Rectangle.calcModelQuads(width,height)

        for transform in [t1,t2,t3,t4,t5,t6]:
            for quad in r:
                a,b,c,d = quad
                aa = t3d.applyTransform(transform, a)
                bb = t3d.applyTransform(transform, b)
                cc = t3d.applyTransform(transform, c)
                dd = t3d.applyTransform(transform, d)
                quads.append((aa,bb,cc,dd))

        return quads

class World(object):
    def __init__(self, transformer):
        self.transformer = transformer
        self.objects = []

    def addObject(self, obj):
        self.objects.append(obj)

    def deleteObject(self, obj):
        self.objects.remove(obj)

    def getPolygons(self, lookat=False, frustum=False, viewport=False):
        polygons = []
        for obj in self.objects:
            objPolygons = obj.model

            objPolygonsT = self.transformer.transform(objPolygons, obj.modelT, lookat=lookat, frustum=frustum, viewport=viewport)

            polygons += objPolygonsT
        return polygons
