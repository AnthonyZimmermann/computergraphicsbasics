import numpy as np
import curses
import time

class Screen(object):
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.pixels = np.zeros((height,width))

    def initScreen(self):
        self.stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()
        self.stdscr.keypad(True)

        self.stdscr.clear()

    def closeScreen(self):

        self.stdscr.keypad(False)
        curses.echo()
        curses.nocbreak()
        curses.endwin()

    def draw(self):
#        lookup = [' ', '-', ':', 'o', 'x', '#']
        lookup = ['.', '~', '+', '/', 'O', '@']
        dynRange = len(lookup)-1

        self.normalizePixels()

        try:
            for i,l in enumerate(self.pixels[::-1]):
                self.stdscr.addstr(i, 0, " ".join( [lookup[int(dynRange*c)] for c in l] ))
                self.stdscr.refresh()
        finally:
            pass

    def setPixel(self, x,y, value):
        if 0 <= x < self.width and 0 <= y < self.height:
            self.pixels[y,x] = max(min(value, 1), 0)

    def addPixel(self, x,y, value):
        if 0 <= x < self.width and 0 <= y < self.height:
            self.pixels[y,x] += max(min(value, 1), 0)

    def setPixels(self, pixels):
        self.pixels = np.zeros((self.height,self.width))
        self.stdscr.clear()
        for pixel in pixels:
            self.setPixel(*pixel, 1)

    def normalizePixels(self):
        self.pixels /= np.max(self.pixels)
