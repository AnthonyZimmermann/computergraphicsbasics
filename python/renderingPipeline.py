from collections import deque
from rasterization import LineRasterization
from transformations import Transformations3D as t3d

class Transformer(object):
    def __init__(self, cameraSettings, viewportSettings):
        self.setCameraTransform(cameraSettings)
        self.setViewportTransform(viewportSettings)

    def setCameraTransform(self, cameraSettings):
        cameraPosition  = cameraSettings["cameraPosition"]
        viewPoint       = cameraSettings["viewPoint"]
        cameraAngle     = cameraSettings["cameraAngle"]
        ratio           = cameraSettings["cameraRatio"]
        nearPlane       = cameraSettings["nearPlane"]
        farPlane        = cameraSettings["farPlane"]

        self.lookatT  = t3d.getLookAtTransform(cameraPosition, viewPoint)
        self.frustumT = t3d.getFrustumTransform(cameraAngle, ratio, nearPlane, farPlane)

    # this viewport cuts one pixel at each boundary to get rid of clipping artifacts
    def setViewportTransform(self, viewportSettings):
        ox,oy = viewportSettings["origin"]
        origin = (ox+2,oy+2)
        width  = viewportSettings["width"]-5 # one pixel per side due to removal of clipping artifacts
        height = viewportSettings["height"]-5 # one pixel per side due to removal of clipping artifacts
#        ox,oy = viewportSettings["origin"]
#        origin = (ox-1,oy-1)
#        width  = viewportSettings["width"]+1 # one pixel per side due to removal of clipping artifacts
#        height = viewportSettings["height"]+1 # one pixel per side due to removal of clipping artifacts

        self.viewportT = t3d.getViewportTransform(origin, width, height)

    def getModelViewTransform(self, modelT):
        return self.lookatT @ modelT

    def getModelViewFrustumTransform(self, modelT):
        return self.frustumT @ self.lookatT @ modelT

    def transform(self, polygons, modelT=None, lookat=False, frustum=False, viewport=False):
        transf = t3d.getIdentityTransform()
        if modelT is not None:
            transf = modelT @ transf
        if lookat:
            transf = self.lookatT @ transf
        if frustum:
            transf = self.frustumT @ transf
        if viewport:
            transf = self.viewportT @ transf

        polygonsT = []
        for polygon in polygons:
            polygonT = []
            for point in polygon:
                polygonT.append(t3d.applyTransform(transf, point))
            polygonsT.append(polygonT)
        return polygonsT

# clipping against unit cube
class Clipper(object):
    def __init__(self):
        ocCacheLen      = 16
        self.ocVertices = deque(list(), maxlen=ocCacheLen)
        self.ocCodes    = deque(list(), maxlen=ocCacheLen)
        self.leavingPoint = None

    def getOutcode(self, v):
        if v in self.ocVertices:
            return self.ocCodes[ self.ocVertices.index(v) ]
        x,y,z = v

        outcode = 0b0
        if x <= -1:
            outcode |= 1
        outcode <<= 1
        if x >= 1:
            outcode |= 1

        outcode <<= 1
        if y <= -1:
            outcode |= 1
        outcode <<= 1
        if y >= 1:
            outcode |= 1

        self.ocVertices.append(v)
        self.ocCodes.append(outcode)

        return outcode

    def outcodeTest(self, p0, p1):
        c0 = self.getOutcode(p0)
        c1 = self.getOutcode(p1)

        trivialReject = (c0 & c1) != 0
        trivialAccept = (c0 | c1) == 0

        if trivialReject:
            return "trivialReject"
        elif trivialAccept:
            return "trivialAccept"
        else:
            return c0 ^ c1

    def sutherlandHodgemanClip(self, p0,p1, clipPlane="+x"):
        if (self.getOutcode(p0) | self.getOutcode(p1)) == 0:
            return [p1]
        combinedOutcode = self.getOutcode(p0) & self.getOutcode(p1)

        x0,y0,z0 = p0
        x1,y1,z1 = p1
        x01,y01,z01 = x1-x0,y1-y0,z1-z0

        def calcAlpha(b, v0, v1):
            return (b-v0)/(v1-v0)

        if "y" in clipPlane:
            if "-" in clipPlane:
                a = calcAlpha(-1, y0, y1)
                if (combinedOutcode & 0b0010) > 0 or not (0 <= a <= 1):
                    return None
            else:
                a = calcAlpha(1, y0, y1)
                if (combinedOutcode & 0b0001) > 0 or not (0 <= a <= 1):
                    return None

            if (clipPlane == "+y" and y01 > 0) or (clipPlane == "-y" and y01 < 0): # leaving
                pp1 = (x0+(a*x01),y0+(a*y01),z0+(a*z01))
                return [pp1]
            if (clipPlane == "+y" and y01 < 0) or (clipPlane == "-y" and y01 > 0): # entering
                pp0 = (x0+(a*x01),y0+(a*y01),z0+(a*z01))
                return [pp0,p1]
        else:
            if "-" in clipPlane:
                a = calcAlpha(-1, x0, x1)
                if (combinedOutcode & 0b1000) > 0 or not (0 <= a <= 1):
                    return None
            else:
                a = calcAlpha(1, x0, x1)
                if (combinedOutcode & 0b0100) > 0 or not (0 <= a <= 1):
                    return None

            if (clipPlane == "+x" and x01 > 0) or (clipPlane == "-x" and x01 < 0): # leaving
                pp1 = (x0+(a*x01),y0+(a*y01),z0+(a*z01))
                return [pp1]
            if (clipPlane == "+x" and x01 < 0) or (clipPlane == "-x" and x01 > 0): # entering
                pp0 = (x0+(a*x01),y0+(a*y01),z0+(a*z01))
                return [pp0,p1]

    def clipPolygonSutherlandHodgemanPart(self, polygon, clipPlane="+x"):
        numVertices = len(polygon)
        clippedPolygon = []
        for i in range(numVertices):
            j = (i+1) % numVertices
            newV = self.sutherlandHodgemanClip(polygon[i],polygon[j])
            if newV is not None:
                clippedPolygon += newV
        return clippedPolygon

    def clipPolygonSutherlandHodgeman(self, polygon):
        # boundary x = 1
        clippedPolygon = self.clipPolygonSutherlandHodgemanPart(polygon, clipPlane="+x")
        # boundary x = -1
        clippedPolygon = self.clipPolygonSutherlandHodgemanPart(clippedPolygon, clipPlane="-x")
        # boundary y = 1
        clippedPolygon = self.clipPolygonSutherlandHodgemanPart(clippedPolygon, clipPlane="+y")
        # boundary y = -1
        clippedPolygon = self.clipPolygonSutherlandHodgemanPart(clippedPolygon, clipPlane="-y")

        return clippedPolygon

    def liangBarskyClip(self, p0,p1):
        outcodeDiff = self.outcodeTest(p0,p1)
        if outcodeDiff == "trivialReject":
            return None
        elif outcodeDiff == "trivialAccept":
            return [p1]
        else:
            x0,y0,z0 = p0
            x1,y1,z1 = p1
            p01 = (x1-x0,y1-y0,z1-z0)
            x01,y01,z01 = p01
            alpha0 = 0
            alpha1 = 1

            def calcAlpha(b, v0, v1):
                return (b-v0)/(v1-v0)

            # y > 1
            if (outcodeDiff & 1) == 1:
                a = calcAlpha( 1, y0, y1)
                if y01 > 0: # leaving
                    alpha1 = min( a, alpha1 )
                else: # entering
                    alpha0 = max( a, alpha0 )

            outcodeDiff >>= 1 # y < -1
            if (outcodeDiff & 1) == 1:
                a = calcAlpha(-1, y0, y1)
                if y01 < 0: # leaving
                    alpha1 = min( a, alpha1 )
                else: # entering
                    alpha0 = max( a, alpha0 )

            outcodeDiff >>= 1 # x > 1
            if (outcodeDiff & 1) == 1:
                a = calcAlpha( 1, x0, x1)
                if x01 > 0: # leaving
                    alpha1 = min( a, alpha1 )
                else: # entering
                    alpha0 = max( a, alpha0 )

            outcodeDiff >>= 1 # x < -1
            if (outcodeDiff & 1) == 1:
                a = calcAlpha(-1, x0, x1)
                if x01 < 0: # leaving
                    alpha1 = min( a, alpha1 )
                else: # entering
                    alpha0 = max( a, alpha0 )


            if alpha0 > alpha1: # should never happen
                return None
            elif alpha0 > 0: # 'entering' or 'entering and leaving'
                pp0 = (x0+(alpha0*x01),y0+(alpha0*y01),z0+(alpha0*z01))
                pp1 = (x0+(alpha1*x01),y0+(alpha1*y01),z0+(alpha1*z01))
                ppC = None

                if self.leavingPoint:
                    def floatEqual(f1,f2):
                        return abs(f1-f2) < 0.000000001
                    lx,ly,lz = self.leavingPoint
                    ex,ey,ez = pp0

                    ppCx,ppCy = 0,0

                    if floatEqual(lx, 1) != floatEqual(ex, 1):
                        ppCx += 1
                    if floatEqual(lx,-1) != floatEqual(ex,-1):
                        ppCx -= 1
                    if floatEqual(ly, 1) != floatEqual(ey, 1):
                        ppCy += 1
                    if floatEqual(ly,-1) != floatEqual(ey,-1):
                        ppCy -= 1

                    if ppCx != 0 and ppCy != 0:
                        ppC = (ppCx,ppCy,(lz+ez)/2)


                self.leavingPoint = None
                if alpha1 < 1:
                    self.leavingPoint = pp1
                if ppC is not None:
                    return [ppC,pp0,pp1]
                return [pp0,pp1]
            elif alpha1 < 1: # 'leaving'
                pp1 = (x0+(alpha1*x01),y0+(alpha1*y01),z0+(alpha1*z01))
                self.leavingPoint = pp1
                return [pp1]

    def clipPolygonLiangBarsky(self, polygon):
        numVertices = len(polygon)
        clippedPolygon = []
        for i in range(numVertices):
            j = (i+1) % numVertices
            newV = self.liangBarskyClip(polygon[i],polygon[j])
            if newV is not None:
                clippedPolygon += newV
        return clippedPolygon

    def clipPolygons(self, polygons):
        clippedPolygons = []
        for polygon in polygons:
            clippedPolygon = self.clipPolygonLiangBarsky(polygon)
#            clippedPolygon = self.clipPolygonSutherlandHodgeman(polygon)
            if len(clippedPolygon) > 0:
                clippedPolygons.append(clippedPolygon)
        return clippedPolygons
if __name__ == "__main__":
    import time
    import signal
    import sys
    import random

    if random.random() < 0.5:
        mode = "quads"
    else:
        mode = "triangles"
    FPS = 10

    screen = Screen(90,50)
#    screen = Screen(160,80)
    screen.initScreen()

    def sigintHandler(sig, frame):
        screen.closeScreen()
        sys.exit(0)
    signal.signal(signal.SIGINT, sigintHandler)

    worldModel = []
    if mode == "quads":
        worldModel += Cuboid.getModelQuads(1,1,1)
    else:
        worldModel += Cuboid.getModel(1,1,1)

    cameraSettings = {
            "cameraPosition"            : (1,1.2,2),
#            "cameraPosition"            : (2,2,2),
            "viewPoint"                 : (0,0,0),
            "cameraAngle"               : 45,
            "cameraRatio"               : screen.width/screen.height,
            "nearPlane"                 : 1,
            "farPlane"                  : 50 }

    viewportSettings = {
            "origin"            : (0,0),
            "width"             : screen.width,
            "height"            : screen.height }
