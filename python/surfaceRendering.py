from models import Cuboid
from models import World
from screen import Screen
from rasterization import LineRasterization
from transformations import Transformations3D as t3d
from renderingPipeline import Transformer
from renderingPipeline import Clipper

class MeshRenderer(object):
    def __init__(self, worldModel, cameraSettings, viewportSettings):
        self.worldModel = worldModel
#        self.transformer = Transformer(cameraSettings, viewportSettings)

    def render(self):
        polygons = self.worldModel.getPolygons(lookat=True, frustum=True)

        # clipping
        clipper = Clipper()
        polygons = clipper.clipPolygons(polygons)

        polygons = self.worldModel.transformer.transform(polygons, viewport=True)

        pixels = []
        for polygon in polygons:
            numVertices = len(polygon)
            for i in range(numVertices):
                j = (i+1) % numVertices
                pixels += LineRasterization.rasterize(polygon[i],polygon[j])

        return pixels

if __name__ == "__main__":
    import time
    import signal
    import sys
    import random

    if random.random() < 0.5:
        mode = "quads"
    else:
        mode = "triangles"
    FPS = 10

#    screen = Screen(90,50)
    screen = Screen(160,80)
    screen.initScreen()

    def sigintHandler(sig, frame):
        screen.closeScreen()
        sys.exit(0)
    signal.signal(signal.SIGINT, sigintHandler)

    cameraSettings = {
#            "cameraPosition"            : (1,1.2,2),
            "cameraPosition"            : (2,2.4,4),
            "viewPoint"                 : (0,0,0),
            "cameraAngle"               : 30,
            "cameraRatio"               : screen.width/screen.height,
            "nearPlane"                 : 1,
            "farPlane"                  : 50 }

    viewportSettings = {
            "origin"            : (0,0),
            "width"             : screen.width,
            "height"            : screen.height }

    transformer = Transformer(cameraSettings, viewportSettings)
    worldModel = World(transformer)
    if mode == "quads":
        cuboidObject1 = Cuboid(1,1,1, mode="quads")
        cuboidObject2 = Cuboid(2,2,2, mode="quads")
    else:
        cuboidObject1 = Cuboid(1,1,1, mode="triangles")
        cuboidObject2 = Cuboid(2,2,2, mode="triangles")
    worldModel.addObject(cuboidObject1)
    worldModel.addObject(cuboidObject2)

    renderer = MeshRenderer(worldModel, cameraSettings, viewportSettings)

    rotX1 = 0
    rotY1 = 0
    rotZ1 = 0
    rotX2 = 0
    rotY2 = 0
    rotZ2 = 0
    rotZYX_T1 = t3d.getRotationTransform(rotZ1, "z") @ t3d.getRotationTransform(rotY1, "y") @ t3d.getRotationTransform(rotX1, "x")
    rotZYX_T2 = t3d.getRotationTransform(rotZ2, "z") @ t3d.getRotationTransform(rotY2, "y") @ t3d.getRotationTransform(rotX2, "x")
    tChange = time.time() + 10
    while True:
        t0 = time.time()+(1/FPS)

        if time.time() > tChange:
            tChange = time.time() + (random.random()*20)
            rotX1 = (random.random()-0.5) * 20
            rotY1 = (random.random()-0.5) * 20
            rotZ1 = (random.random()-0.5) * 20

            rotX2 = (random.random()-0.5) * 20
            rotY2 = (random.random()-0.5) * 20
            rotZ2 = (random.random()-0.5) * 20

        rotZYX_T1 = t3d.getRotationTransform(rotZ1, "z") @ t3d.getRotationTransform(rotY1, "y") @ t3d.getRotationTransform(rotX1, "x")
        rotZYX_T2 = t3d.getRotationTransform(rotZ2, "z") @ t3d.getRotationTransform(rotY2, "y") @ t3d.getRotationTransform(rotX2, "x")

        cuboidObject1.transform(rotZYX_T1)
        cuboidObject2.transform(rotZYX_T2)
        pixels = renderer.render()

        screen.setPixels(pixels)
        screen.draw()

        waitTime = t0 - time.time()
        if waitTime > 0:
            load = int( (1-(waitTime/(1/FPS))) * 100 )
            print("\n", end="\r")
            print("FPS: {}, load: {}%{}".format(FPS, load," "*10), end="\r")
            time.sleep(waitTime)
